#define _GNU_SOURCE 1
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "seccomp-bpf.h"

typedef void (*Call)(void);

static int install_syscall_filter(void)
{
	struct sock_filter filter[] = {
		VALIDATE_ARCHITECTURE,
		EXAMINE_SYSCALL,
		ALLOW_SYSCALL(exit),
		ALLOW_SYSCALL(read),
		ALLOW_SYSCALL(write),
		ALLOW_SYSCALL(open),
		KILL_PROCESS,
	};
	struct sock_fprog prog = {
		.len = (unsigned short)(sizeof(filter)/sizeof(filter[0])),
		.filter = filter,
	};
	if(prctl(PR_SET_NO_NEW_PRIVS,1,0,0,0)){
		perror("prctl(NO_NEW_PRIVS)");
		goto failed;
	}
	if(prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, &prog)){
		perror("prctl(SECCOMP)");
		goto failed;
	}
	return 0;
failed:
	if(errno == EINVAL)
		fprintf(stderr, "SECCOMP_FILTER is not available\n");
	return 1;
}
int main(void)
{
	char buf[101]={0},k;
	int i;
    Call call;
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin,NULL,_IONBF,0);
    printf("Wellcome to Hacker's Typer\n");
    for(i=0;i<100;i++)
    {
       	k=getc(stdin);
       	if(k == 0x48 || k == 0x0a)
          	break;
       	buf[i] = k;
    }
	call = (Call)buf;
    if(install_syscall_filter())
		return 1;
    call();
	return 0;
}